# Spritly

Like Spreeder; it helps you speed read.

## Features maybe?

- Have a syntax for timing
    - you could use `--` to add a wait after a paragraph
- Save long docs for shared studying?
    - could use [hashify](https://github.com/hashify/hashify.me)

## Alternatives
Some other projects that are similar

- https://github.com/cameron/squirt
- https://github.com/danneu/elm-speed-reader